# Duke Nukem Forever 2001

The Duke Nukem Forever 2001 Source Code (Excluding the game assets + game build)

The `main` branch has the vanilla Duke Nukem Forever code without any fixes or changes

## Getting started

Compiling the source code under windows xp

1. Install Visual C++ 6.0 https://archive.org/details/microsoftvisualstudio60 (product code 110-1234567)
2. Install Visual C++ 6.0 SP5 and then the Processor Pack https://archive.org/details/vcpp5
3. Go to System Properties -> Advanced -> Environment Variables and set the following:
   * BUILD_ROOT_DUKE = path to game files
   * C32BIN = C:\Program Files\Microsoft Visual Studio\VC98\Bin
   * C32RC = C:\Program Files\Microsoft Visual Studio\Common\MSDev98\Bin
   * MASM_BIN = C:\Program Files\Microsoft Visual Studio\VC98\Bin
4. Copy the contents of Patches\KNI Assembly\ into the root directory.
5. Open Duke4.dsw and repeatedly choose cancel followed by no until no more prompts appear.
6. Go to Tools -> Options -> Directories
7. Add DirectX8\Inc\ to the top of the include files list.
8. Add DirectX8\Lib\ to the top of the library files list.
9. Save the workspace and build with Build -> Rebuild All.
10. Inside the System folder, run "del *.u & ucc make -nobind"